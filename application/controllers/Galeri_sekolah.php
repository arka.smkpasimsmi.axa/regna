<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri_sekolah extends CI_Controller {

	public function listFoto()
	{
		$title['title'] = 'List Foto';
		$data = [
			'foto'	=> $this->m_galeri->getFoto()
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/galeri/list_foto.php',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function listVideo()
	{
		$title['title'] = 'List Video';
		$data = [
			'video'	=> $this->m_galeri->getVideo()
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/galeri/list_video.php',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function detailFoto($id)
	{
		$id = ['id' => $id];
		$nama 		= $this->db->get_where('tb_m_galeri',$id)->row_array();
		$title['title'] = 'Detail Foto | '.$nama['judul'];
		$data = [
			'galeri'	=> $this->crud->getById('tb_m_galeri',$id)
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/galeri/detail_foto.php',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function detailVideo($id)
	{
		$id = ['id' => $id];
		$nama 		= $this->db->get_where('tb_m_galeri',$id)->row_array();
		$title['title'] = 'Detail Video | '.$nama['judul'];
		$data = [
			'galeri'	=> $this->crud->getById('tb_m_galeri',$id)
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/galeri/detail_video.php',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function deleteGaleri($id,$menu)
	{
		$this->crud->delete($id,'tb_m_galeri');
		$this->session->set_flashdata('success','Sukses hapus data!');
		if ($menu == 'foto') {
			Redirect('Galeri_sekolah/listFoto');
		}elseif ($menu== 'video') {
			Redirect('Galeri_sekolah/listVideo');
		}
	}

	public function postEditVideo($ids) {
		$id 				= ['id' => $ids];
		$judul				= $this->input->post('judul');
		$deskripsi 			= $this->input->post('deskripsi');
		$item 				= $this->input->post('item');
			
			$data = [
				'item'				=> $item,
				'judul' 			=> $judul,
				'deskripsi'			=> $deskripsi
			];
			$this->crud->edit($id,$data,'tb_m_galeri');
			$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
			redirect(base_url('Galeri_sekolah/detailVideo/').$ids);
		
	}

	public function insertVideo()
	{
		$judul 			= $this->input->post('judul');
		$item			= $this->input->post('item');
		$deskripsi 		= $this->input->post('deskripsi');

		$data =[
			'item'			=> $item,
			'judul'			=> $judul,
			'deskripsi' 	=> $deskripsi,
			'created_by'	=> 'ADMIN',
			'kategori'		=> 'video'
		];

		$this->crud->insert($data,'tb_m_galeri');
		$this->session->set_flashdata('success', 'Data berhasil Ditambahkan!');
		Redirect('Galeri_sekolah/listVideo');
	}

	public function insertFoto()
	{
		$this->form_validation->set_rules('judul','Judul', 'required',
    		['required' => 'Judul harus diisi!']);
		$this->form_validation->set_rules('deskripsi','Deskripsi', 'required',
    		['required' => 'Deskripsi harus diisi!']);

		if ($this->form_validation->run()== false) {
			$title['title'] = 'List Foto';
			$data = [
				'foto'	=> $this->m_galeri->getFoto()
				];

			$this->load->view('templates/server_partial/script_css',$title);
			$this->load->view('templates/server_partial/header');
			$this->load->view('templates/server_partial/sidebar');
			$this->load->view('server/front_end/galeri/list_foto.php',$data);
			$this->load->view('templates/server_partial/footer');
			$this->load->view('templates/server_partial/script_js');
		}else{
			$judul			= $this->input->post('judul');
			$deskripsi		= $this->input->post('deskripsi');

				$config['upload_path']		= './assets/images/galeri_images/';
				$config['allowed_types']	= 'jpg|png|jpeg';
				$config['file_name']		= $judul.'-'.date('y-m-d');
				$this->load->library('upload', $config);

				if(!$this->upload->do_upload('foto')){
					$this->session->set_flashdata('fail', 'Kesalahan mengunggah gambar!');
    				Redirect('Galeri_sekolah/listFoto');
				}else{
					$foto 	  = $this->upload->data('file_name');
					$data = [
						'judul'		=> $judul,
						'item'		=> $foto,
						'deskripsi'	=> $deskripsi,
						'kategori'	=> 'foto',
						'created_by'=> 'ADMIN'
					];
					$this->crud->insert($data,'tb_m_galeri');
					$this->session->set_flashdata('success' , 'Pendaftaran Berhasil!');
					Redirect('Galeri_sekolah/listFoto');
			}
		}

	}


	public function postEditFoto($ids) {
		$id 			= ['id' => $ids];
		$judul			= $this->input->post('judul');
		$deskripsi 		= $this->input->post('deskripsi');
		$foto 			= $this->input->post('foto');
		$foto_lama 		= $this->input->post('foto_lama');

		if ($foto !== '') {
			$config['upload_path']		= './assets/images/galeri_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['file_name']		= $judul.'-'.date('y-m-d');
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('foto')){
				$foto = $foto_lama;
				$hapus = 'false';
    		}else{
    			$upload_data	= $this->upload->data();
    			$foto 	  		= $upload_data['file_name'];
    			$hapus = 'true';
   			}

		}

			$data = [
				'item'				=> $foto,
				'judul'				=> $judul,
				'deskripsi'			=> $deskripsi
			];
			if ($hapus == 'true') {
				$this->crud->deletePhoto($foto_lama,'galeri_images');
			}
			$this->crud->edit($id,$data,'tb_m_galeri');
			$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
			redirect(base_url('Galeri_sekolah/detailFoto/').$ids);	
		
	}
}