<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_guru extends CI_Model {

	public function getAllGuru() {
		$this->db->join('auth','auth.guru_id=tb_m_guru.id','left');
		$this->db->select('tb_m_guru.*, auth.username as email');
		return $this->db->get('tb_m_guru')->result();
	}

	public function insert($data, $table) {
		$this->db->insert($table, $data);
	}

	public function edit($id, $data, $table) {
		$this->db->where($id);
		$this->db->update($table, $data);
	}

	public function delete($id, $table) {
		$this->db->where_in('id', $id);
		$this->db->delete($table);
	}

	public function getGuruLimit() {
		$this->db->order_by('created_dt', 'DESC');
		return $this->db->get('tb_m_guru', 4)->result();
	}

	public function getGuruProfile($id)
	{
	    $this->db->join('auth','auth.guru_id=tb_m_guru.id','left');
		$this->db->select('tb_m_guru.*, auth.username as email');
		$this->db->where('tb_m_guru.id',$id);
		return $this->db->get('tb_m_guru');
	}

}    
