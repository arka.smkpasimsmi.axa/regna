<section id="team">
  <div class="container" data-aos="fade-up">
    <div class="row">
      <?php foreach ($guru as $d): ?>
      <div class="col-lg-3 col-md-6">
        <div class="member" data-aos="fade-up" data-aos-delay="100">
          <div class="pic"><img src="<?= base_url('assets/images/guru_images/'.$d->foto) ?>" alt="" height="250" width="100%"></div>
          <h4><?= $d->nama_guru ?></h4>
          <span><?= $d->mapel ?></span>
          <div class="social">
            <?php if ($d->facebook == " ") {
                        $facebook ="web.facebook.com";
                    }else{
                        $facebook ="$d->facebook";
                    } 
                ?>
            <a href="<?= $facebook ?>" target="_blank"><i class="fa fa-facebook"></i></a>
            <a href="<?= 'https://www.instagram.com/'.$d->instagram ?>" target="_blank"><i class="fa fa-instagram"></i></a>
          </div>
        </div>
      </div>
      <?php endforeach ?>
    </div>

  </div>
</section><!-- End Team Section -->