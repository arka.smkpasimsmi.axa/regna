<!-- ======= Portfolio Details Section ======= -->
    <section class="portfolio-details" data-aos="fade-up">
      <div class="row">
      	<?php foreach ($bkk as $d): ?>
		<?php if ($d->kategori == "lowongan"): ?>
      	<div class="col-12 col-md-6 col-lg-4">
	        <div class="portfolio-details-container card">

	          <div class="owl-carousel portfolio-details-carousel" >
	          	<a href="<?= base_url('assets/images/bkk_images/'.$d->item); ?>">
	            	<img src="<?= base_url('assets/images/bkk_images/'.$d->item) ?>" alt="Mobirise" title="" media-simple="true" width="100%" height="300" >
	       		</a>
	          </div>

	        </div>

	        <div class="portfolio-description" style="margin-top: -30px;">
	          <a href="<?= base_url('Page/detailPageById/detailLowongan/'.$d->id) ?>"><h2><?= $d->judul ?></h2></a>
	          <p>
	            <?= $d->deskripsi ?>
	          </p>
	        </div>
	    </div>
	    <?php endif ?>
	<?php endforeach ?>
      </div>
    </section><!-- End Portfolio Details Section
    -->
</div>