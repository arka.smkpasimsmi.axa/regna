<?php foreach ($jurusan as $d): ?>
<!-- ======= Portfolio Details Section ======= -->
    <section class="portfolio-details">
      <div class="container">

        <div class="portfolio-details-container">

          <div class="owl-carousel portfolio-details-carousel">
            <img src="<?= base_url('assets/images/jurusan_images/'.$d->foto) ?>" class="img-fluid" alt="">
          </div>

          <div class="portfolio-info">
            <h3><?= $d->nama_jurusan ?></h3>
            <ul class="mb-4">
              <li><?= $d->deskripsi_jurusan ?></li>
            </ul>
          </div>

        </div>
      </div>
    </section><!-- End Portfolio Details Section -->
<?php endforeach ?>   
  
