<?php 
    if($this->session->userdata('role') !== 'admin')  {
    Redirect('Page/pageNotFound');
    } 
?>
<?php foreach($prestasi as $d) : ?>
<form method="POST" action="<?= base_url('Admin_guru/deleteGuru/'.$d->id) ?>" id="form-delete" ></form>
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-file-text bg-blue"></i>
                <div class="d-inline">
                    <h5>Detail Prestasi</h5>
                    <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="../index.html"><i class="ik ik-home"></i></a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">Pages</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">Profile</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 col-md-5">
        <div class="card">
            <div class="card-body">
                <div class="text-center"> 
                    <a href="<?= base_url('assets/images/prestasi_images/'.$d->foto) ?>" class="single-popup-photo">
                        <img src="<?= base_url('assets/images/prestasi_images/'.$d->foto) ?>" class="img-thumbnail" width="150" height="150">
                    </a>
                    <h4 class="card-title mt-10"><?= $d->nama_prestasi ?></h4>
                    <p class="card-subtitle text-dark">Tanggal :<?= $d->tanggal_prestasi ?></p>
                    <button class="btn btn-danger" type="submit" id="btn-delete-submit">Hapus Data</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-8 col-md-7">
        <div class="card">
    <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active show" id="pills-deskripsi-tab" data-toggle="pill" href="#last-month" role="tab" aria-controls="pills-deskripsi" aria-selected="true">Deskripsi</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-setting-tab" data-toggle="pill" href="#previous-month" role="tab" aria-controls="pills-setting" aria-selected="false">Edit</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade active show" id="last-month" role="tabpanel" aria-labelledby="pills-deskripsi-tab">
            <div class="card-body">
                <div class="row">
                    <h4 class="ml-3">Deskripsi Prestasi</h4>
                </div>
                <div class="border-bottom"></div>
                <p class="mt-20"><?= $d->deskripsi ?></p>
            </div>
        </div>
        <div class="tab-pane fade" id="previous-month" role="tabpanel" aria-labelledby="pills-setting-tab">
            <div class="card-body">
                <form method="POST" action="<?= base_url('Admin_prestasi/postEditPrestasi/'.$d->id) ?>" id="form-edit" enctype="multipart/form-data">
                    <input type="hidden" name="foto_lama" value="<?= $d->foto ?>">
                    <div class="form-group">
                        <a href="<?= base_url('assets/images/prestasi_images/'.$d->foto) ?>" class="single-popup-photo">
                            <img src="<?= base_url('assets/images/prestasi_images/'.$d->foto) ?>" class="rounded-circle" width="150" height="150">
                        </a>
                    </div>
                    <div class="form-group">
                        <label>Foto</label>
                        <input type="file" class="form-control file-upload-info" placeholder="Upload Image" name="foto" >
                        <small>(Biarkan kosong jika tidak ingin diganti)</small>
                    </div>
                    <div class="form-group">
                        <label for="example-name">Nama Kejuaraan</label>
                        <input type="text" placeholder="Johnathan Doe" class="form-control" name="nama_prestasi" id="example-name" value="<?= $d->nama_prestasi ?>">
                    </div>
                <div class="form-group">
                    <label for="exampleTextarea1">Tanggal</label>
                    <input id="dropper-default" class="form-control" type="text" placeholder="Tanggal Prestasi" readonly="readonly" name="tgl_prestasi" value="<?= $d->tanggal_prestasi ?>">
                </div>
                <div class="form-group">
                    <label for="exampleTextarea1">Deskripsi</label>
                    <textarea class="form-control" id="exampleTextarea1" rows="4" name="deskripsi"><?= $d->deskripsi ?></textarea>
                </div>
                
                    <button class="btn btn-success" type="submit" id="btn-edit-submit">Update Data</button>
                </form>
            </div>
        </div>
    </div>
</div>
    </div>
</div>
<?php endforeach; ?>