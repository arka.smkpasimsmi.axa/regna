<?php 
    if($this->session->userdata('role') !== 'admin')  {
    Redirect('Page/pageNotFound');
    } 
?>
<?php foreach ($galeri as $d): ?>
	<?php $url = $d->item;  ?>
    <?php $value = explode("v=", $url); ?>
    <?php $videoId = $value[1]; ?>
<form method="POST" action="<?= base_url('Galeri_sekolah/deleteGaleri/'.$d->id.'/video') ?>" id="form-delete" ></form>
<form method="POST" action="<?= base_url('Galeri_sekolah/postEditVideo/'.$d->id) ?>" id="form-edit" enctype="multipart/form-data">
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <a href="<?= base_url('Galeri_sekolah/listVideo') ?>"><i class="ik ik-arrow-left" style="background-color: #14bdee;"></i></a>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <ul class="nav nav-pills custom-pills" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active show" id="pills-detail-tab" data-toggle="pill" href="#current-month" role="tab" aria-controls="pills-detail" aria-selected="true">Detail</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#last-month" role="tab" aria-controls="pills-profile" aria-selected="false">Edit</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade active show" id="current-month" role="tabpanel" aria-labelledby="pills-detail-tab">
            <div class="card-body">
				<div class="row feature_row">
					<!-- Feature Content -->
					<div class="col-lg-6 feature_col">
						<div class="feature_content">
							<div class="accordions">
								<div class="elements_accordions">
									<div class="accordion_container">
										<h3 class="section_title"><?= $d->judul ?></h3>
										<div class="border-bottom"></div>
										<div class="section_subtitle mt-2">
											<p><?= $d->deskripsi ?></p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 feature_col">
						<div class="feature_video d-flex flex-column align-items-center justify-content-center">
						<iframe width="100%" height="200px" src="https://www.youtube.com/embed/<?= $videoId; ?>"></iframe>
						</div>
					</div>
				</div>
			</div>
        </div>
        <div class="tab-pane fade" id="last-month" role="tabpanel" aria-labelledby="pills-profile-tab">
            <div class="card-body">
				<div class="row feature_row">
					<!-- Feature Content -->
					<div class="col-lg-6 feature_col">
						<div class="feature_content">
							<div class="accordions">
								<div class="elements_accordions">
									<div class="accordion_container">
										<h3><input name="judul" type="text" value="<?= $d->judul ?>"></h3>
										<div class="border-bottom"></div>
										<div class=" mt-2">
											<textarea name="deskripsi" class="form-control" rows="10"><?= $d->deskripsi ?></textarea>
											<button class="btn btn-warning mt-2" type="submit" id="btn-edit-submit">Simpan Perubahan</button>
											<button class="btn btn-danger mt-2" type="submit" id="btn-delete-submit">Hapus Video</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 feature_col">
							<iframe width="100%" height="200px" src="https://www.youtube.com/embed/<?= $videoId; ?>"></iframe>
							<div class="form-group mt-2">
			                    <label>Link Video</label>
			                    <div class="input-group">
			                        <input name="item" type="text" value="<?= $d->item ?>" class="form-control">
			                    </div>
                			</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
</form>
<?php endforeach ?>