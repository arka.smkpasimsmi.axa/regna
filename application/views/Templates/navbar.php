<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <?php foreach ($navbar as $d): ?>
      <title><?= $d->nama_sekolah.' - '. $title ?></title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?= base_url('assets/img/'.$d->ikon) ?>" rel="icon">
  <link href="<?= base_url('assets/img/'.$d->ikon) ?>" rel="apple-touch-icon">
    <?php endforeach ?>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Poppins:300,400,500,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?= base_url() ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="<?= base_url() ?>assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?= base_url() ?>assets/vendor/magnific-popup/dist/magnific-popup.css">
  <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/server_assets/plugins/magnific-popup/dist/magnific-popup.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?= base_url('assets/server_assets/plugins/jquery-toast-plugin/dist/jquery.toast.min.css') ?>">
  <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/icon-kit/dist/css/iconkit.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/ionicons/dist/css/ionicons.min.css">

  <!-- =======================================================
  * Template Name: Regna - v2.1.0
  * Template URL: https://bootstrapmade.com/regna-bootstrap-onepage-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left row">
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt=""></a> -->
         <!-- Uncomment below if you prefer to use a text logo -->
         <?php foreach ($navbar as $d): ?>
           <img style="width:50px;" src="<?= base_url('assets/img/'.$d->ikon) ?>" alt="">
          <h1><a href="#hero" class="ml-2"><?= $d->teks_logo ?></a></h1> 
         <?php endforeach ?>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li ><a href="<?= base_url() ?>">Beranda</a></li>
          <li class="menu-has-children <?= $this->uri->segment(3) == "detailStruktur" || $this->uri->segment(3) == "ListJurusan" ? 'menu-active' : ' '?>"><a href="#">Profil</a>
            <ul>
              <li><a href="<?= base_url('Page/detailPage/detailStruktur') ?>">Struktur Organisasi</a></li>
              <li><a href="<?= base_url('Page/detailPage/ListJurusan') ?>">Jurusan Sekolah</a></li>
            </ul>
          </li>
          <li class="menu-has-children <?= $this->uri->segment(3) == "ListFoto" || $this->uri->segment(3) == "ListVideo" ? 'menu-active' : ' '?>"><a href="#">Galeri</a>
            <ul>
              <li><a href="<?= base_url('Page/detailPage/ListFoto') ?>">Foto</a></li>
              <li><a href="<?= base_url('Page/detailPage/ListVideo') ?>">Video</a></li>
            </ul>
          </li>
          <li class="<?= $this->uri->segment(3) == "ListGuru" ? 'menu-active' : ' '?>"><a href="<?= base_url('Page/detailPage/ListGuru') ?>">Guru</a></li>
          <li class="<?= $this->uri->segment(3) == "ListPrestasi" ? 'menu-active' : ' '?>"><a href="<?= base_url('Page/detailPage/ListPrestasi') ?>">Prestasi</a></li>
          <li class="<?= $this->uri->segment(3) == "detailPpdb" ? 'menu-active' : ' '?>"><a href="<?= base_url('Page/detailPage/detailPpdb') ?>">PPDB</a></li>
          <li class="menu-has-children <?= $this->uri->segment(3) == "ListLowongan" || $this->uri->segment(3) == "ListSerapan" ? 'menu-active' : ' '?>"><a href="#">BKK</a>
            <ul>
              <li><a href="<?= base_url('Page/detailPage/ListLowongan') ?>">Lowongan Kerja</a></li>
              <li><a href="<?= base_url('Page/detailPage/ListSerapan') ?>">Serapan Kerja</a></li>
            </ul>
          </li>
          <li class="<?= $this->uri->segment(3) == "detailKontak"? 'menu-active' : ' ' ?>"><a href="<?= base_url('Page/detailPage/detailKontak') ?>">Hubungi Kami</a></li>
          <li class="menu-has-children"><a href="#">Login</a>
            <ul class="dropdown-menu dropdown-menu-right mt-2" style="width: 150px;">
               <li class="px-3 py-2">
                   <form action="<?= base_url('Page/login') ?>" method="POST">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="email" class="form-control form-control-sm" name="email" value="<?= set_value('email'); ?>" autofocus>
                        <small style="font-style: italic;"><?= '<style>small > p{color:red;}</style>', form_error('email'); ?></small>
                      </div>
                      <div class="form-group mt-3">
                        <label for="exampleInputPassword1">Password</label>
                        <input type="password" class="form-control form-control-sm" name="password">
                        <small style="font-style: italic;"><?= '<style>small > p{color:red;}</style>', form_error('password'); ?></small>
                      </div>
                      <!-- <div class="row"> -->
                        <!-- <div class="text-left">
                          <a href="" ><span style="font-size: 10px;">Lupa password?</span></a>
                        </div> -->
                        <div class="ml-4 text-right">
                          <button type="submit" class="btn btn-primary text-sm-center mt-3 "><i class="ik ik-log-in"></i></button>
                        </div>
                      <!-- </div> -->
                    </form>
                </li>
            </ul>
          </li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- End Header -->
